-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 20, 2021 at 12:48 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oway_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2020_05_21_100000_create_teams_table', 1),
(7, '2020_05_21_200000_create_team_user_table', 1),
(8, '2020_05_21_300000_create_team_invitations_table', 1),
(9, '2021_01_19_095439_create_sessions_table', 1),
(10, '2021_01_19_111148_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'testing', '5e1a5835e0d3e77dd5fe65b0f49197a3c4ffb32591d321464d3d253aceaec056', '[\"read\",\"create\",\"update\",\"delete\"]', NULL, '2021-01-19 03:41:37', '2021-01-19 03:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Miss', 'Quod voluptatem est ut nam totam est aut. At voluptatum expedita ea ab nemo explicabo. Corrupti maiores unde ratione.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(2, 'Mr.', 'Omnis voluptatum magni odit ut in beatae. Atque enim nostrum recusandae ad ad sit. Hic est assumenda voluptates iste sunt exercitationem a.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(3, 'Dr.', 'Neque exercitationem nisi qui. Sit voluptas voluptas facere nesciunt itaque. Eius et sapiente ab illum eaque maiores. Fuga qui iure harum cumque dolores.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(4, 'Prof.', 'Deleniti quam beatae itaque et quia ea magni. Qui repellendus excepturi qui. Saepe quam sed at quidem. Itaque earum architecto nisi ratione ut.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(5, 'Prof.', 'Nihil nihil sit animi sunt alias numquam. Corrupti at totam labore voluptatem ea. Ut harum occaecati suscipit soluta rerum. Reiciendis explicabo ad tempora dolor.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(6, 'Ms.', 'Eveniet atque eum harum repudiandae quia velit laudantium. Voluptas velit corporis voluptatum cumque quo officia.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(7, 'Mrs.', 'Sunt sit iure vel libero. Quia blanditiis autem nostrum non libero. Corporis sequi a maxime cum. Culpa laborum mollitia animi expedita.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(8, 'Mrs.', 'Quas sequi ullam maiores cumque quis. Ratione repudiandae aspernatur tenetur tenetur impedit nostrum. Laborum velit quidem omnis. Rerum repellendus eos quia id voluptas.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(9, 'Dr.', 'Ipsam est quaerat voluptatem quidem illum. At sunt eos quis iste corporis. Delectus qui voluptatem cum nihil iure. Debitis earum consectetur sapiente ducimus voluptate ullam et.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(10, 'Dr.', 'Aut ut qui inventore ut nihil modi quam. Excepturi voluptatem fuga quisquam ea illo vel. Aperiam cumque modi quos et reprehenderit.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(11, 'Dr.', 'Quia temporibus fugiat vel tempora blanditiis. Perspiciatis explicabo soluta odit eveniet. Vitae aut rerum reprehenderit excepturi animi dolor. Eum nulla assumenda ullam quis facilis ipsa.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(12, 'Prof.', 'Veritatis harum labore sit odio perferendis repellendus ipsam laboriosam. Et eum dolores nostrum quia.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(13, 'Dr.', 'Iure eveniet dolor itaque dignissimos molestiae sunt. Vel autem expedita eos tenetur veniam. Quasi et assumenda aliquam in enim quia eum. Placeat quo corrupti molestiae deleniti quisquam.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(14, 'Ms.', 'Dolorem totam ratione suscipit inventore quia facere. Dicta id qui consequatur velit possimus. Asperiores dolores dolorem rerum corrupti.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(15, 'Prof.', 'Qui est accusamus distinctio sapiente harum debitis. Iusto pariatur velit est in quibusdam adipisci harum temporibus. Doloremque aut iure rem repellat. Incidunt sit provident sunt.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(16, 'Mrs.', 'Et qui quibusdam vero ad fuga deserunt est. Repudiandae possimus rerum dolore aut.', '2021-01-19 05:24:23', '2021-01-19 05:24:23'),
(17, 'Dr.', 'Qui quos quia enim illo quaerat hic. Voluptates ut neque quidem quaerat sapiente et ut ex. Omnis eligendi minima corrupti earum sint perferendis. Corporis rerum sit ratione est velit non.', '2021-01-19 05:24:23', '2021-01-19 05:24:23');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('mHAgUuMsUTEsjAcXRJcXdXqcotQbaTXl1fEHrI2n', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRUFCdkFrSlA3eHpDYzlvbEJwTVVVMjNnS1lUQnRhR1FpWkZJSG92cSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1611076346),
('NqHsDouociLHd3V7tWqv1u3peb8WTMgobzSgcOCj', 2, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiT0k0RXNqSFVrUEdjV1JuZDRweGRTNk9oRU9Oa0xCUXVtU3RPZlRFdSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9vd2F5LnRlc3QiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToyO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkZGY1N1pkMUpnZ2p0NjJWakxKUDJOdXVUS2I0elFKeXh4VXJ6Zngxb2hmM0VSVDZDQUNSd20iO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJGRmNTdaZDFKZ2dqdDYyVmpMSlAyTnV1VEtiNHpRSnl4eFVyemZ4MW9oZjNFUlQ2Q0FDUndtIjt9', 1611077891);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `user_id`, `name`, `personal_team`, `created_at`, `updated_at`) VALUES
(1, 1, 'Test\'s Team', 1, '2021-01-19 03:33:19', '2021-01-19 03:33:19'),
(2, 2, 'testing2\'s Team', 1, '2021-01-19 07:02:06', '2021-01-19 07:02:06');

-- --------------------------------------------------------

--
-- Table structure for table `team_invitations`
--

CREATE TABLE `team_invitations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_invitations`
--

INSERT INTO `team_invitations` (`id`, `team_id`, `email`, `role`, `created_at`, `updated_at`) VALUES
(2, 1, 'thantzin.abcmib@gmail.com', 'editor', '2021-01-19 03:44:19', '2021-01-19 03:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `team_user`
--

CREATE TABLE `team_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'test@test.com', NULL, '$2y$10$l/x34Verc8tjzTQJfIH73uBYE8PYf26m3KDAPBfsfuqZ8fW9lfWDW', 'eyJpdiI6IllBVnJDZ2JqdVdHMEdBbDZJWWljOEE9PSIsInZhbHVlIjoiNTlvVVMyNDdaSitjSzNpbDRBUG1YWVZJK1hndnZULzhrNVRENjNuTndZVT0iLCJtYWMiOiI4MjJlZjZhYjQwN2NjMmRkNGIxMDZlNGFmYzk2YjcwMWI5Y2JhNTYxOTk0ZjVmNjMxZjNmMjE2ZWY3ZTYyYzMxIn0=', 'eyJpdiI6IktPT3NSRmVwdWJsV0J2K2Y1MmhKaWc9PSIsInZhbHVlIjoibVZ1c0R4b0NpVzhIUkh2Zk5qVmZWakRkV2YyYkVrMEFZU1pvR1Z5MHBTU3RSZm5ZUnVxMXl1QytPRXh0UEw2VVQxVG9wdVpNL3pqYmJ5QlEyYWRqSmpId2laS0hXLzB2QWV2TGN0RHgvSVNra2EwRzBidkxqdzFOZjZ5eWtoVThSZmpJVzkrcndtcEN6M09GRnhVLzlrOU9sazBVcFp4Q1JtN0VBaGp5ZlByM3h4KzZ3NlFLYnlQcVRORTQ3eTg3eU1lSlFiVjZtRUViRXZ6TVdUWnYxUWMrYjNRZDBlTm9uUWgxandYQU9SNzFEekJsbUFEYWdqd2ZybUpoOGpPOFduakp0RzNTVlAxRy9zTkdCOSs5QXc9PSIsIm1hYyI6ImQ0YmFhODEwNTZiZDBlY2IzNzkxMDAwMDMxMTY4NGE4ZmFkZWI4ZGE0NWUxNWFkMTMyNTdmNzIxYmMwMGE1MDkifQ==', NULL, 1, 'profile-photos/fj5FmNJwDAhgWbbP3RwggmYmdR2u4nu5uBZfA5x6.png', '2021-01-19 03:33:19', '2021-01-19 04:38:57'),
(2, 'testing2', 'test2@gmail.com', NULL, '$2y$10$df57Zd1Jggjt62VjLJP2NuuTKb4zQJyxxUrzfx1ohf3ERT6CACRwm', NULL, NULL, NULL, NULL, 'profile-photos/VmyFve0k4rd5MCSkUjWX807iRlODs08gbMfyCvnT.png', '2021-01-19 07:02:06', '2021-01-19 09:33:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teams_user_id_index` (`user_id`);

--
-- Indexes for table `team_invitations`
--
ALTER TABLE `team_invitations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_invitations_email_unique` (`email`),
  ADD KEY `team_invitations_team_id_foreign` (`team_id`);

--
-- Indexes for table `team_user`
--
ALTER TABLE `team_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `team_invitations`
--
ALTER TABLE `team_invitations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `team_user`
--
ALTER TABLE `team_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `team_invitations`
--
ALTER TABLE `team_invitations`
  ADD CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
