<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;

class PostController extends Controller
{
    public function posts(){
    	$posts_data = Post::paginate(10);

    	return view('home', ['posts_data'=>$posts_data]);
    }

    public function json_posts(){
    	$json_posts = Post::paginate(10);

    	return json_encode($json_posts);
    }

    public function json_users(){
    	$json_users = User::paginate(10);

    	return json_encode($json_users);
    }
}
